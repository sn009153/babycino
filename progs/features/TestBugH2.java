class TestBugH2 {
    public static void main(String[] args) {
        int x = 0;
        int y = 10;
        do {
            y = y - 1;
            x = x + 1;
        } while (x == 0); // check condtion at the start
        System.out.println("y: " + y);
    }
}
