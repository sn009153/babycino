class TestBugH3 {
    public static void main(String[] args) {
        int x = 5;
        int y = 0;
        do {
            y++;
        } while (x > 0); //condition will always be true
        System.out.println("y: " + y);
    }
}
